#!/usr/bin/python -tt
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

"""Wordcount exercise
Google's Python class

The main() below is already defined and complete. It calls print_words()
and print_top() functions which you write.

1. For the --count flag, implement a print_words(filename) function that counts
how often each word appears in the text and prints:
word1 count1
word2 count2
...

Print the above list in order sorted by word (python will sort punctuation to
come before letters -- that's fine). Store all the words as lowercase,
so 'The' and 'the' count as the same word.

2. For the --topcount flag, implement a print_top(filename) which is similar
to print_words() but which prints just the top 20 most common words sorted
so the most common word is first, then the next most common, and so on.

Use str.split() (no arguments) to split on all whitespace.

Workflow: don't build the whole program at once. Get it to an intermediate
milestone and print your data structure and sys.exit(0).
When that's working, try for the next milestone.

Optional: define a helper function to avoid code duplication inside
print_words() and print_top().

"""

import sys
import re

# +++your code here+++
# Define print_words(filename) and print_top(filename) functions.
# You could write a helper utility function that reads a file
# and builds and returns a word/count dict for it.
# Then print_words() and print_top() can just call the utility function.

TOP = 20


def remove_ls(input_string):
    """function that receives a string
    and returns it without leading spaces"""
    no_ls = input_string
    if len(input_string) > 0:
        i = 0
        while input_string[i] == ' ' and i < len(input_string) - 1:
            i += 1
        no_ls = input_string[i:]
    return no_ls


def clean_inner_spaces(input_string):
    """get rid of multiple inner spaces,
    as well as all trailing spaces"""
    if len(input_string) == 0:
        return input_string
    string_list = [input_string[0]]

    for i in range(1, len(input_string)):
        if input_string[i] != " " or (input_string[i] ==
                                      " " and input_string[i-1] != " "):
            string_list.append(input_string[i])
    i = len(string_list) - 1
    while string_list[i] == " ":
        i -= 1
    if i < len(string_list) - 1:
        string_list = string_list[:i + 1]

    return ''.join(string_list)


def separate_characters(input_string):

    """separates punctuations from words"""
    for punctuation in re.findall("[?,.!-:()\"`\[\]\{\}'*;_]+\s | \s[?,.!-:()\"`\[\]\{\}'*;_]+|\s[0-9]+\s", input_string):
        input_string = input_string.replace(punctuation," ")
        #input_string = re.replace("[?,.!-:()\"`\[\]\{\}'*;_]\s | \s[?,.!-:()\"`\[\]\{\}'*;_]|\s[0-9]\s", input_string)
    input_string = input_string.replace("\n", " ")
    return input_string


def get_sorted_dictionary(filename):
    """reads text file, returns dictionary of word/punctuation frequencies
    as well as set of all words/punctuations"""
    try:
        file_reader = open(filename, 'r')
        story_string = remove_ls(file_reader.read())
    except:
        print("error: unable to open file")
        return []
    story_string = clean_inner_spaces(separate_characters(story_string))
    story_string = story_string.lower()
    file_reader.close()
    story_list = story_string.split(' ')
    story_dict = {}
    for story_index in range(len(story_list)):
        if story_list[story_index] in story_dict.keys():
            story_dict[story_list[story_index]] += 1
        else:
            story_dict[story_list[story_index]] = 1
    word_set = set(story_list)
    word_set = sorted(word_set)
    return [word_set, story_dict]


def print_words(filename):
    """reads text file,prints all words/punctuations in alphabetical order"""
    sorted_story = get_sorted_dictionary(filename)
    if len(sorted_story) == 0:
        return
    word_set = sorted_story[0]
    story_dict = sorted_story[1]

    for word in word_set:
        print(word, story_dict[word])


def print_top(filename):
    """reads text file, prints the top-20
    most frequent words (or punctuations)"""
    sorted_story = get_sorted_dictionary(filename)
    if len(sorted_story) == 0:
        return
    story_dict = sorted_story[1]
    reverse_story_dict = {}
    for key in story_dict.keys():
        reverse_story_dict[story_dict[key]] = key

    down_counter = TOP
    next_key = max(reverse_story_dict.keys())
    while down_counter > 0:
        if next_key in reverse_story_dict.keys():
            print(reverse_story_dict[next_key], next_key)
            down_counter -= 1
        next_key -= 1


###

# This basic command line argument parsing code is provided and
# calls the print_words() and print_top() functions which you must define.
def main():
    if len(sys.argv) != 3:
        print('usage: ./wordcount.py {--count | --topcount} file')
        sys.exit(1)

    option = sys.argv[1]
    filename = sys.argv[2]
    if option == '--count':
        print_words(filename)
    elif option == '--topcount':
        print_top(filename)
    else:
        print('unknown option: ' + option)
        sys.exit(1)


if __name__ == '__main__':
    main()
