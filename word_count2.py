#!/usr/bin/python -tt
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

"""Wordcount exercise
Google's Python class

The main() below is already defined and complete. It calls print_words()
and print_top() functions which you write.

1. For the --count flag, implement a print_words(filename) function that counts
how often each word appears in the text and prints:
word1 count1
word2 count2
...

Print the above list in order sorted by word (python will sort punctuation to
come before letters -- that's fine). Store all the words as lowercase,
so 'The' and 'the' count as the same word.

2. For the --topcount flag, implement a print_top(filename) which is similar
to print_words() but which prints just the top 20 most common words sorted
so the most common word is first, then the next most common, and so on.

Use str.split() (no arguments) to split on all whitespace.

Workflow: don't build the whole program at once. Get it to an intermediate
milestone and print your data structure and sys.exit(0).
When that's working, try for the next milestone.

Optional: define a helper function to avoid code duplication inside
print_words() and print_top().

"""

import sys


# +++your code here+++
# Define print_words(filename) and print_top(filename) functions.
# You could write a helper utility function that reads a file
# and builds and returns a word/count dict for it.
# Then print_words() and print_top() can just call the utility function.
def remove_ls(input_string):
    """function that receives a string
    and returns it without leading spaces"""
    no_ls = input_string
    if len(input_string) > 0:
        i = 0
        while input_string[i] == ' ' and i < len(input_string) - 1:
            i += 1
        no_ls = input_string[i:]
    return no_ls

def clean_inner_spaces(input_string):
    """get rid of multiple inner spaces,
    as well as all trailing spaces"""
    if len(input_string) == 0:
        return input_string
    string_list = [input_string[0]]

    for i in range(1, len(input_string)):
        if input_string[i] != " " or (input_string[i] == " " and input_string[i-1] != " "):
            string_list.append(input_string[i])
    i = len(string_list) - 1
    while string_list[i] == " ":
        i -= 1
    if i < len(string_list) - 1:
        string_list = string_list[:i + 1]

    return ''.join(string_list)



def separate_characters(input_string):
    for punctuation in ",.!-:(\"":
        input_string = input_string.replace(punctuation, " " + punctuation + " ")
    input_string = input_string.replace("\n", " ")
    return input_string


def print_words(filename):
    """creates a dictionary of words in file,
  values being word counts"""
    file_reader = open(filename, 'r')
    story_string = remove_ls(file_reader.read())
    story_string = clean_inner_spaces(separate_characters(story_string))
    story_string = story_string.lower()
    file_reader.close()
    story_list = story_string.split(' ')
    #print(story_list)
    story_dict = {}
    for story_index in range(len(story_list)):
        if story_list[story_index] in story_dict.keys():
            story_dict[story_list[story_index]] += 1
        else:
            story_dict[story_list[story_index]] = 1
    story_list.sort()
    print(set(story_list))
    #return story_dict
    #for sorted_words in story_list.sort():
    #    print(sorted_words, story_dict[sorted_words])


print_words('alice.txt')


