def print_words(filename):
  """creates a dictionary of words in file,
  values being word counts"""
  file_reader = open(filename, 'r')
  story_string = file_reader.read()
  story_list = []
  for story_index in range(len(story_string)):
    if story_string[story_index].isalnum() or story_string[story_index] == " ":
      story_list.append(story_string[story_index].lower())
  word_list = str(story_list).split(" ")
  word_dict = {}
  print(word_list)
  for word in word_list:
    if word not in word_dict.keys():
      word_dict[word] = 1
    else:
      word_dict[word] += 1
  print(word_dict)
  return word_dict

print(print_words("alice.txt"))
